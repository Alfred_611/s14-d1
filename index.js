// Single line comments - (Shortcut ctrl +/(windows) or cmd +/(mac))
/*
	Multi line comments
	- covers multiple line of comments
	- (Shortcut ctrl + shft + /(widnwos))
	- Shortcut cmd + opt + / (Mac))
*/ 


// Syntax and Statements
/*
	- Statements in programming are instructions that we tell the computer to perform.
	- JS Statements usually end with semicolon(;) although in JS (;) is not required. But for beginners this is a good practice.
	- Syntax in programming, it is the set of rules that describes how statements must be constructed. siya nagchecheck kung paano cinoconstruct si statements.
*/

console.log("Hello Batch 138!");

// Variables
/*
	- It is used to contain data.
	- Declaring variables - tells our devices that a variable name is created and is ready to store data
	- Syntax:
		let/const variableName; always in camel case
		when we use let keyword - used to handle values that will change over time.
		const [constant] kewyord - used to handle values that are constant. (example is pi)
*/

let myVariable;

console.log(myVariable);

//console.log(hello);

//Initializing values to variables
/*
	- Syntax:
	let/const variableName = value;
*/

let productName = 'desktop computer';

console.log(productName);

let productPrice = 18999;

console.log(productPrice);

/* Reassigning variable values
	- change the initial or previous value into another value
*/

productName = 'Laptop';

console.log(productName); //di na kailangan ulitin ang let

const interest = 3.539;

console.log(interest);

/*interest = 4.489;

console.log(interest);
*/

let supplier;

supplier = "John Smith Tradings"; //pag words ang ginagamit use "" pero both works fine

console.log(supplier);

// Declaring multiple variables
let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode, productBrand);

// Data types
	
/*Strings
	- strings are series of characters that create a word, a phrase, a sentence or anything related to creating a text
	- strings in JS can be written either a single (') or double quotes (")
*/
let country = 'Philippines';
let province = "Metro Manila";

// String concatenation
/*combining multiple string values to create a single string using the 
+ symbol;*/
// Output: Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress)

// Output: Welcome to Metro Manila, Philippines
console.log("Welcome to " + fullAddress + "!")

//escape character
// \n - refers to creating a new line in between text
let mailAddress = "Metro Manila\n\nPhilippines";

console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

//Numeric Data Types
//Integers/Whole numbers
let headcount = 26;
console.log(headcount);

//Decimal Numbers a.k.a. floating points
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
// Output: John's grade last quarter is 98.7
console.log("John's grade last quarter is " + grade);

//Boolean
//1 / 0 or True/False
let isMarried = true;
let isGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Array
// Array is a special kind of data type that is used to store multiple values
// Arrays can store different data types but is normally used to store similar data types
/* Syntax
	let/const arrayName = [element0, element1, element 2, ...]
*/
let grades = [98.7, 92.1, 90.2, 94.6]; //values nito ang tawag ay 'elements'
console.log(grades);

/* Objects - yung values ang tawag dito ay 'properties'
	- another special kind of data type that's used to mimic real world objects/items
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09195678910", "09992828822"],
	address: {
		houseNumber: 458,
		city: "Marikina"
	}
}

console.log(person);

// Null vs. Undefined.
/* Null
	- used to intentionally express the absence of a value in a variable declaration. yung data type natin was assigned to a variable but does not hold any value or amount.
*/
let spouse = null;
let money = 0;
let myName = "";

/* Undefined
	- represents the state of a variable that has been declared but without an assigned value
*/
let fullName;

/* Functions
	- Functions in JS are lines /blocks of codes that tell our device/application to perform a certain task when called/invoked 

		Declaring functions
		Syntax:
		function functionName(){
			line/block of codes;
		}
*/

//Declaring a function
function printName(){
	console.log("My name is John");
}

// Invoking/Calling a function
//printName();

// Declare a function that will display your favorite animal
function favoriteAnimal(){
	console.log("Wombat");
}

favoriteAnimal();

// (name) - parameter
// Parameter - acts as a named variable/container that exists only inside of a function
function printName(name){
	console.log("My name is " + name);
}
// Argument - the actual value that is provided a function for it to work properly
printName("John");


printName("Camille");
function favoriteDotaHero(hero){
	console.log("My favorite dota hero is " + hero);
}
favoriteDotaHero("Morphling");

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// Finding more information about a function in the console
console.log(argumentFunction);

// Using multiple parameters

function createFullName(firstName, middleName, lastName){
	console.log("Hello " + firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Pablo", "Cruz");
createFullName("Juan", "Pablo",);
createFullName("Juan", "Pablo", "Cruz", "Junior");

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// the "return" statement
// - this statement allows the output of a function to be passed to the line/block of code that invoked/called the function
function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("A simple message");
}

let completeName = returnFullName(firstName, middleName, lastName);
console.log(completeName);

console.log(returnFullName(firstName, middleName, lastName))


/*declaring - declare a variable (declare muna bago initialize)
	  initializing - assign a value to a specific variable*/



/*never start a variable with a number. ok lang kung spelled out
 never use special characters as part of the name of your variable*/